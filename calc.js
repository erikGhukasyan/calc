class Calc {
    digit = "";
    toDisplay = 0;
    operator;
    result;
    isFloat = false;
    equalEvent = false;
    operatorEvent = false;


    display(sign) {
        if(this.equalEvent) {
            this.operator = undefined;
            this.equalEvent = false;
        }
        if(this.digit.includes(".")) {
            this.isFloat = true;
        }
        if(sign === "+" || sign === "-" || sign ===  "*" || sign === "/") {
            this.operator = sign;
            this.digit = "";
            this.isFloat = false;
        }
        else if(sign === "." && this.isFloat) {
            return this.digit;
        }
        else {
            this.digit = this.digit + sign;
        }
        return this.digit;
    }

    restart() {
        this.equalEvent = false;
        this.digit = "";
        this.toDisplay = 0;
        this.operator = undefined;
        this.result = 0;
        this.isFloat = false;
    }

    operation(num1, num2) {
        this.operatorEvent = false;
        this.equalEvent = true;
        this.digit = "";
        this.isFloat = false;
        this.result = num1;

        switch(this.operator) {
            case '+':
                this.result = this.result + num2;
                break;
            case '-':
                this.result = this.result - num2;
                break;
            case '*':
                this.result = this.result * num2;
                break;
            case '/':
                this.result = this.result / num2;
        }

        return this.result;
    }




}